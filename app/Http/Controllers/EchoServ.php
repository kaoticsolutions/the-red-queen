<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class EchoServ extends BaseController {

	public function fail() {
		return \Alexa::say("Sorry, I cannot do that.")->endSession();
	}

	public function test($test) {
		return \Alexa::say($test)->endSession();
	}

	public function uptime() {
		return \Alexa::say(`uptime`)->endSession();
	}
}
