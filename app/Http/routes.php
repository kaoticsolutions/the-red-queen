<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

AlexaRoute::launch('/echo', 'App\Http\Controllers\EchoServ@fail');
AlexaRoute::intent('/echo', 'GetTestIntent', 'App\Http\Controllers\EchoServ@test');
AlexaRoute::intent('/echo', 'GetUptimeIntent', 'App\Http\Controllers\EchoServ@uptime');